<?php

namespace Drupal\phone_international\Element;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a phone_international form.
 *
 * Usage example:
 *
 * By default field has geolocation enable.
 *
 * @code
 * $form['phone'] = [
 *   '#type' => 'phone_international',
 *   '#title' => $this->t('International Phone'),
 * ];
 * @endcode
 *
 * If you want default country you need to do this:
 *
 * @code
 * $form['phone'] = [
 *   '#type' => 'phone_international',
 *   '#title' => $this->t('International Phone'),
 *   '#attributes' => [
 *      'data-country' => 'PT',
 *      'data-geo' => 0, // 0(Disable) or 1(Enable)
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("phone_international")
 */
class PhoneInternationalElement extends FormElement {

  /**
   * Form element validation handler for #type 'color'.
   */
  public static function validateNumber(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = trim($element['#value']);
    $form_state->setValueForElement($element, $value);
    if ($value !== '' && !Drupal::service('phone_international.validate')
      ->isValidNumber($value)) {
      $form_state->setError($element, t('The %name "%phone_international" is not valid.', [
        '%phone_international' => $value,
        '%name' => $element['#title'],
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderInternationalPhone($element) {
    $element['#attributes']['type'] = 'tel';
    $element['#attributes']['class'][] = 'phone_international-number';

    $element['#attached']['library'][] = 'phone_international/phone_international';

    // Add auto geolocation.
    if (!array_key_exists('data-geo', $element['#attributes'])) {
      $element['#attributes']['data-geo'] = 1;
    }

    // Add default country.
    if (!array_key_exists('data-country', $element['#attributes'])) {
      $element['#attributes']['data-country'] = 'PT';
    }

    // Get Module path to load utilsTellInput.js.
    $module_handler = Drupal::service('module_handler');
    $module_path = $module_handler->getModule('phone_international')->getPath();
    $element['#attached']['drupalSettings']['phone_international']['path'] = $module_path;

    Element::setAttributes($element, [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
      'disabled',
      'pattern',
    ]);
    static::setAttributes($element, ['form-tel']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 30,
      '#maxlength' => 128,
      '#process' => [
        [$class, 'processAutocomplete'],
        [$class, 'processAjaxForm'],
        [$class, 'processPattern'],
      ],
      '#element_validate' => [
        [$class, 'validateNumber'],
      ],
      '#pre_render' => [
        [$class, 'preRenderInternationalPhone'],
      ],
      '#theme' => 'input__tel',
      '#theme_wrappers' => ['form_element'],
    ];
  }

}
