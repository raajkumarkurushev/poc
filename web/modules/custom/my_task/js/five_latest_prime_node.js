(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.fiveLatestPrimeNode = {
    attach: function (context, settings) {
      $('.block-five-latest-prime-node table', context).once('fiveprimenode').addClass('five-prime-node-table-view');
    }
  };

})(jQuery, Drupal, drupalSettings);
