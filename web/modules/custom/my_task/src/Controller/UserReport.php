<?php

namespace Drupal\my_task\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

/**
 * Contains a form for switching the view mode of a node during preview.
 *
 * @internal
 */
class UserReport extends ControllerBase {
  
  /**
   * {@inheritdoc}
   */
  public function content() {
    $user = \Drupal::routeMatch()->getParameter('user');
    
    $key = $user->field_unique_id_key->value;
    $allottedCredit = $user->field_allotted_credits->value;
    $approvedCredit = $user->field_approved_credits->value;
    
    $value = $this->getUserValue($key);
    
    $user->set('field_allotted_credit_balance', [$value]);
    
    $usedCredit = (int) $allottedCredit - $value;
    $user->set('field_used_credits', [$usedCredit]);
    
    $approvedCreditBalance = (int) $approvedCredit - $usedCredit;
    $user->set('field_approved_credit_balance', [$approvedCreditBalance]);
    
    $user->save();
    
    $key = $user->field_unique_id_key->value;
    $approvedCredit = $user->field_approved_credits->value;
    $UsedCredit = $user->field_used_credits->value;
    $approvedCreditBal = $user->field_approved_credit_balance->value;
    
    return [
      '#theme' => 'report',
      '#data' => [
        'sender_id' => $baseUrl,
        'unique_id_key' => $key,
        'approved_credits' => $approvedCredit,
        'approved_credit_balance' => $approvedCreditBal,
        'alloted_credits' => $allottedCredit,
        'user_credits' => $UsedCredit,
      ],
    ];
  }
  
  public function getUserValue($key) {
    $url = 'http://sms.acsitsolutions.in/api/balance.php?authkey=' . $key . '&type=4';
    $data = file_get_contents($url);
    return json_decode($data, TRUE);

    $client  = new GuzzleClient();
    $request = new GuzzleRequest('GET', $url);
    $response = $client->send($request, ['timeout' => 30]);
    
    return json_decode($response->getBody());
  }
}
