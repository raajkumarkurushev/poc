<?php

namespace Drupal\my_task\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a block with five Latest Prime Node
 *
 * @Block(
 *   id = "five_latest_prime_node",
 *   admin_label = @Translation("Five Latest Prime Node"),
 * )
 */
class FiveLatestPrimeNode extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    $nodes = $this->entityTypeManager->getStorage('node')
      ->loadByProperties(['status' => 1]);
    
    $primeNodes = [];
    foreach($nodes as $node) {
      $publishedTime = $node->getCreatedTime();
      $publishedDay = date('d', $publishedTime);
      
      $publishedDayPrimeCheck = $this->primeCheck($publishedDay);
      
      if($publishedDayPrimeCheck) {
        $primeNodes[] = [
          'data' => [
            $node->getType(),
            $node->getTitle(),
            date('m-d-Y', $publishedTime),
            $node->toLink('View', 'canonical'),
          ],
        ];
      }
      
      if(count($primeNodes) == 5)
        break;
    }
    
    $header = [
      'Type',
      'Title',
      'Published Date',
      'View',
    ];
    
    return [
      '#theme' => 'table', 
      '#header' => $header,
      '#rows' => $primeNodes,
      '#empty' => $this->t('No Records found ...'),
      '#attached' => [
        'library' => [
          'my_task/five_latest_prime_node'
        ],
      ],
    ];
  }

  /**
   * Check Prime Number
   */
  public static function primeCheck($number) {
    if ($number == 1) 
    return false; 
    for ($i = 2; $i <= $number/2; $i++){ 
    if ($number % $i == 0) 
    return false; 
    } 
    return true; 
  } 
  
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
  
  public function getCacheMaxAge() {
    return 0;
  }
}