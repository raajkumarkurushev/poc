<?php

namespace Drupal\my_task\Form;

use Drupal\Core\Controller\ControllerBase;

/**
 * Contains a form for switching the view mode of a node during preview.
 *
 * @internal
 */
class UserReport extends FormBase {
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "user_report";
  }
  
  /**
   * ID of the node.
   *
   */
  protected $node;
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $node = null) {
    $this->node = $node;
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $currentPublishedTimestamp = $this->node->getCreatedTime();
    $status = 0;
    
    $currentPublishedDay = (int) date('d', $currentPublishedTimestamp);
    $currentPublishedMonth = date('m', $currentPublishedTimestamp);
    $currentPublishedYear = date('Y', $currentPublishedTimestamp);
    
    $currentPublishedDayPrimeCheck = FiveLatestPrimeNode::primeCheck($currentPublishedDay);
    
    // if(!$currentPublishedDayPrimeCheck) {
      for($day = $currentPublishedDay-1; $day >= 1; $day--) { 
        
        if(FiveLatestPrimeNode::primeCheck($day)) {
          $timestamp = strtotime($currentPublishedMonth . '/' . $day .'/' . $currentPublishedYear);
          
          $this->node->setCreatedTime($timestamp);
          $status = $this->node->save();
          Cache::invalidateTags($this->node->getCacheTags());
          break;
        } 
      }
    // }
    
    if(!$status) {
      $this->messenger()
        ->addWarning($this->t('No Change in Publish date'));
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Date has been changed to Prime published date'));
    }
    
    $form_state->setRedirect(
      'entity.node.canonical',
      ['node' => $this->node->id()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.node.canonical', ['node' => $this->node->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you want to make %title to be prime publish date?', ['%title' => $this->node->getTitle()]);
  }
}
