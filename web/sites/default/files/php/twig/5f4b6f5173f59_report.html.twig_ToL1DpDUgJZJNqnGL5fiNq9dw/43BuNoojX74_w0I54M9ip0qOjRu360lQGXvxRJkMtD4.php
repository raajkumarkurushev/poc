<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/my_task/templates/report.html.twig */
class __TwigTemplate_f618dd2fdff6b94b7666daceda61cecd717f88d2f95c83865903648255e3547c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"row col\">

  <div class=\"card col\">
    <div class=\"card-body text-center\">
      <h2 class=\"card-title\"><strong>Approved Credit</strong></h2>
      <h2 class=\"text-center\" id=\"approved_credit\"><strong>";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["data"] ?? null), "approved_credits", [])), "html", null, true);
        echo "</strong></h2>
    </div>
  </div>

  <div class=\"card col\">
    <div class=\"card-body text-center\">
      <h2 class=\"card-title\"><strong>User Credit</strong></h2>
      <h2 class=\"text-center\" id=\"approved_credit\"><strong>";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["data"] ?? null), "user_credits", [])), "html", null, true);
        echo "</strong></h2>
    </div>
  </div>

  <div class=\"card col\">
    <div class=\"card-body text-center\">
      <h2 class=\"card-title\"><strong>Approved Credit Balance</strong></h2>
      <h2 class=\"text-center\" id=\"approved_credit\"><strong>";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["data"] ?? null), "approved_credit_balance", [])), "html", null, true);
        echo "</strong></h2>
    </div>
  </div>

</div>
  
  ";
    }

    public function getTemplateName()
    {
        return "modules/custom/my_task/templates/report.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 20,  72 => 13,  62 => 6,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/custom/my_task/templates/report.html.twig", "C:\\xampp\\htdocs\\lenishpoc\\web\\modules\\custom\\my_task\\templates\\report.html.twig");
    }
}
